/** \mainpage Realtime Binaural filter with distance modulation: hrir~
	\section Description
	This object receives three parameters (azimuth, elevation, and distance) and
 convolves the audio sent to its audio inlet with a minimum phase and diffused
 field equalized version of the hrir measured by Prof. Qu and colleagues as
 reported at IEEE Trans. on Audio, Speech, and Language processing, 17(6), 2009.
 
	\section Licence
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 Created by Julian Villegas on 10/1/2013.
 
	@author Julian Villegas <julian ^_^ at ^_^ u-aizu (*) ac (*) jp>
	@version 0.2a
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include "m_pd.h"
#include "sqlite3.h"
#define DEFAULT_N_POINTS 256
#define MAX_N_POINTS 3000
#define MAX_BLOCKSIZE 8192
#define DBFILENAME "/HRIR@44100.db"
#define MyPI 3.14159265358979323846

static t_class *hrir_class;

typedef struct _hrir{
    t_object x_obj;
    t_outlet *audio_outputL; /// The convolution of the input with the minimum phase L-hrir
    t_outlet *audio_outputR; /// The convolution of the input with the minimum phase R-hrir
    t_float sr; ///The sampling rate
    t_float azimuth;   ///inlet from 0 to 359 degrees
    t_float elevation; ///inlet from -40 to 90 degree
    t_float distance;     ///inlet from 20 to 90 degree
    t_int nPts;     /// No. of taps used for the convolution
    t_int nPtsInDb; /// No. of taps existing in the database (sampling rate dependent)
    t_float azi; ///azimuth value
    t_float ele; ///elevation value
    t_float rng; ///distance value
    t_float rs[2]; /// nearest measured distance for a given point
    t_float es[2]; /// nearest measured elevation for a given point
    t_float as[2]; /// nearest measured azimuth for a given point
    t_float on[3]; /// flag to indicate when a point concide with a measurement
    t_float set[9][3]; /// measurements candidate for interpolation in  this order: rng, ele, azi
    t_float dist[9]; /// Distances from the measurements to the point
    t_int n_meas; /// number of candidates for interpolation
    
    char path[2000];
    
    t_float prevAzi; ///azimuth value
    t_float prevEle; ///elevation value
    t_float prevRng; ///distance value
    
    t_float crossCoef[MAX_BLOCKSIZE];
    
    t_float previousImpulse[2][MAX_N_POINTS]; // one more for the interpolation
    t_float currentImpulse[9][2][MAX_N_POINTS]; // one more for the interpolation
    int currentRow; // For the IR extraction
    t_float convBufferC[MAX_N_POINTS];
    t_float convBufferI[MAX_N_POINTS];
    
    t_float f;                   /* dummy float for dsp */
    t_int bufferPin;
    sqlite3 *db;
    char *zErrMsg;
    char **pzErrMsg;
    t_int rc;
    t_int connected;
} t_hrir; ///< data structure for the class

#include "hrirFunctions.h"
/**
 * Process the convolution of the incoming audio
 */
static t_int *hrir_perform(t_int *w) {
    t_hrir *x = (t_hrir *)(w[1]);
    t_float *in = (t_float *)(w[2]);
    t_float *outR = (t_float *)(w[3]); // audio out R
    t_float *outL = (t_float *)(w[4]); // audio out L
    int blocksize = (int)(w[5]);
    
    if (x->connected==1) {
        x->prevAzi = x->azi; /// Save last values to skip unnecessary computations
        x->prevEle = x->ele;
        x->prevRng = x->rng;
        
        x->azi = roundf(x->azimuth);
        x->ele = roundf(x->elevation);
        x->rng = roundf(x->distance);
        
        validatePoint(x);
        
        if((x->prevAzi == x->azi) && (x->prevEle == x->ele) && (x->prevRng == x->rng)) {
            //Do not find the filter
        } else {
            rangeDis(x);
            rangeEle(x);
            rangeAzi(x);
            formSet(x);
            testCoplan_lin(x);
        }
        convolve(x, blocksize, in, outL, outR);
    }
    return (w+6);
}

/** Inform Pd that this object does DSP
 * hrir_perform: the callback function
 * 5: number of parameters (listed ahead)
 * x: userdata
 * sp[0]->s_vec: in_samples
 * sp[1]->s_vec: audio_out
 * sp[0]->s_n: blocksize
 */
static void hrir_dsp(t_hrir *x, t_signal **sp) {
    dsp_add(hrir_perform, 5, x,  sp[0]->s_vec, sp[1]->s_vec,  sp[2]->s_vec, sp[0]->s_n);
    x->sr=sp[0]->s_sr;
    int power, base;
    
    char file[2000]="";
    char str[8]="";
    strcpy(file,x->path);
    strcat(file,"/HRIR@");
    sprintf(str, "%d", (int)x->sr);
    strcat(file,str);
    strcat(file,".db");
    if (access(file, F_OK) == -1) { // file doesn't exist
        post("Warning: The database %s doesn't exist,\ntrying to use %s database", file, DBFILENAME);
        strcpy(file,x->path);
        strcat(file,"/HRIR@");
        sprintf(str, "%d", 44100);
        strcat(file,str);
        strcat(file,".db");
        if(access(file, F_OK) == -1){
            post("Error: The database %s doesn't exist either, trying downloading from \n http://arts.u-aizu.ac.jp/research/hrir-with-distance-control/");
        } else {
            post("Warning: Using %s at a sampling freq. of %d, results are not correct.",file, (int)x->sr);
        }
    } else {
        if (x->connected == 1) {
            sqlite3_close(x->db);
            x->connected==0;
        }
        x->rc = sqlite3_open(file, &(x->db));
        if(x->rc) {
            post("Can't open database: %s : %s\n",file ,sqlite3_errmsg(x->db));
            sqlite3_close(x->db);
        } else {
            post("%s database opened",file);
            x->connected=1;
        }
        
        switch ((int)(x->sr)) {
            case 8000:
                // number of taps at 8000 = 125
                x->nPtsInDb = 250;
                break;
            case 16000:
                // number of taps at 16000 = 250
                x->nPtsInDb = 500;
                break;
            case 22050:
                //number of taps at 22050 = 344
                x->nPtsInDb = 688;
                break;
            case 44100:
                // number of taps at 44100 = 689
                x->nPtsInDb = 1378;
                break;
            case 48000:
                // number of taps at 48000 = 750
                x->nPtsInDb = 1500;
                break;
            case 65536:
                // number of taps at 65536 = 1024
                x->nPtsInDb = 2048;
                break;
            case 96000:
                // number of taps at 96000 = 1500
                x->nPtsInDb = 3000;
                break;
            case 192000:
                // number of taps at 192000 = 3000
                x->nPtsInDb = 6000;
                break;
            default:
                post("WARNING: Define the right amount of hrir taps for the  sampling rate: %0.0f Hz.\nConsider downloading a suitable database for this sampling rate.\n", x->sr);
                break;
        }
        
        base = x->nPtsInDb/2;
        power = 0;
        
        while (base > 1) {
            base = base >> 1;
            power +=1;
        }
        base = 1;
        while (power-- > 0) {
            base = base << 1;
        }
        
        if (x->nPts > x->nPtsInDb/2) {
            x->nPts = base;
        }
        
        if (!isPowerOfTwo(x->nPts)) {
            x->nPts = base;
            post("Number of tabs must be a power of 2\n", base, x->nPts);

        }
        post("hrir~: Max. blocksize: 8192, Max. taps %d, currently using %d \n", base, x->nPts);
    }
}

/** Instantiate the object
 *
 * @param azimArg the azimuth
 * @param elevArg the elevation
 * @param distanceArg the distance
 */
static void *hrir_new(t_floatarg nPointsArg) {
    t_hrir *x = (t_hrir *)pd_new(hrir_class);
    x->audio_outputL = outlet_new(&x->x_obj, gensym("signal"));
    x->audio_outputR = outlet_new(&x->x_obj, gensym("signal"));
    floatinlet_new(&x->x_obj, &x->azimuth);     /// azimuth
    floatinlet_new(&x->x_obj, &x->elevation);   /// elevation
    floatinlet_new(&x->x_obj, &x->distance);   /// distance
    
    x->azi = 0;
    x->ele = 180; // so the first time the object is opened we have output
    x->rng = 20;
    x->nPtsInDb = 300;
    
    if (nPointsArg>0) {
        x->nPts = nPointsArg;
    } else {
        x->nPts = DEFAULT_N_POINTS;
    }
    
    for (int i = 0; i < MAX_N_POINTS; i++) {
        x->convBufferC[i] = 0;
        x->convBufferI[i] = 0;
        x->previousImpulse[0][i] = 0;
        x->previousImpulse[1][i] = 0;
    }
    x->bufferPin = 0;
    
    for (int i = 0; i < MAX_BLOCKSIZE; i++) {
        x->crossCoef[i] = 1.0 * i / (MAX_BLOCKSIZE-1.0);
        //x->crossCoef[i] = cos((MyPI/2) * ((float)i/(MAX_BLOCKSIZE-1.0))); ///cosine-sine cross-fading
    }
    
    strcat(x->path,canvas_getdir(canvas_getcurrent())->s_name); /// The files should be in the same directory
    x->connected=0;
    
    return (x);
}

/**
 * close the database when disposing the hrir~ object
 */
static void hrir_free(t_hrir *x) {
    if(x->connected) {
        sqlite3_close(x->db);
        post("measurement database closed");
        x->connected=0;
    }
}

/**
 * setup the object
 */
void hrir_tilde_setup(void) {
    hrir_class = class_new(gensym("hrir~"), (t_newmethod)hrir_new, (t_method)hrir_free,
                           sizeof(t_hrir), CLASS_DEFAULT, A_DEFFLOAT, A_DEFFLOAT, 0);
    CLASS_MAINSIGNALIN(hrir_class, t_hrir, f);
    class_addmethod(hrir_class, (t_method)hrir_dsp, gensym("dsp"), 0);
}