
This library provides sound spatialization via head-related impulse responses
originally recorded by Prof. Qu and colleagues as reported at IEEE TRANSACTIONS ON AUDIO, SPEECH, AND LANGUAGE PROCESSING, VOL. 17, NO. 6, AUGUST 2009. The HRIR used here however, are diffuse field equalized, and minimum-phase filtered. Also, only the right hemisphere is used.

This library depends on sqlite3.
